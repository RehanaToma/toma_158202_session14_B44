<?php
 namespace tap;
 use app\person;

 class student extends person{
     private $studentid;

     public function setStudentid($studentid)
     {
         $this->studentid = $studentid;
     }

     public function getStudentid()
     {
         return $this->studentid;
     }
 }
 ?>